from django.urls import path

from recipes.views import (
    RecipeCreateView,
    RecipeUpdateView,
    RecipeDeleteView,
    RecipeDetailView,
    RecipeListView,
)

urlpatterns = [
    path("", RecipeListView.as_view(), name="recipes_list"),
    path("<int:pk>/", RecipeDetailView.as_view(), name="recipe_detail"),
    path("new/", RecipeCreateView.as_view(), name="recipe_new"),
    path("<int:pk>/edit/", RecipeUpdateView.as_view(), name="recipe_edit"),
    path(
        "<int:recipe_id>/ratings/",
        RecipeDeleteView.as_view(),
        name="recipe_rating",
    ),
]
